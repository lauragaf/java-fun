package com.training.pets;

import com.training.Feedable;

public class Dog extends Pet implements Feedable {

    public Dog(){
     this.setNumLegs(4);
    }
    public Dog (String name){

        this.setName(name);
        this.setNumLegs(4);
    }


    public void feed() {

        System.out.println("Chciken for the doggo!!!");
    }

    public void speed() {

        System.out.println("0-40 in 10 secs");
    }

    public String toString(){
        return "this is a dog with " + this.getNumLegs() +" legs and its name is " +this.getName();
    }
}
