package com.training;

import com.training.person.Person;
import com.training.pets.Dog;

import javax.xml.namespace.QName;

public class InterfacesMain {




    public static void main(String[] args) {

        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person();
        horace.setName("Horace");

        Dog birdie = new Dog();

        birdie.setName("Birdie");
        birdie.speed();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = birdie;


        for (int i = 0; i < thingsWeCanFeed.length; i++) {
            if (thingsWeCanFeed[i] != null) {
                thingsWeCanFeed[i].feed();
            }


            //if i reallly want to make birdie run on a full stomach

            if (thingsWeCanFeed[i] instanceof Dog) {
                ((Dog) thingsWeCanFeed[i]).speed();
            }
        }
    }


}
