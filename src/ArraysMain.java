import com.training.pets.Dog;
import com.training.pets.Pet;

public class ArraysMain {


    public static void main(String[] args) {



        int[] intArray = {5,7,34,77};

        Pet[] petArray = new Pet[10];



        Pet firstPet = new Pet();
        firstPet.setName("Susie");

        petArray[0] = firstPet;


        // put a dog n the array

       Dog myDog = new Dog("Simba");
        petArray[1] = myDog;


// print out the contents of array
        for (int i=0; i<petArray.length; ++i ){
            System.out.println(petArray[i]);
        }

        petArray[1].feed();
        petArray[0].getName();
    }


}
